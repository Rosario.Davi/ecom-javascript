const wrapper = document.querySelector(".sliderWrapper")
const menuItems = document.querySelectorAll(".menuItem")

const products = [
    {
        id:1,
        title: "Fender Startocaster",
        price: 2099.99,
        colors: [
            {
                code: "orange",
                img: "./img/fenderStrat.png"
            },
            {
                code: "pink",
                img:"./img/pinkStrat.png"
            },
        ],
    },

    {
        id: 2,
        title: "Gibson Les Paul",
        price: 2999.00,
        colors: [
            {
            code: "yellow",
            img: "./img/gibsonLesPaul.png",
            },
            {
            code: "gold",
            img: "./img/goldLesPaul.png",
            },
        ],
        },
    {
        id: 3,
        title: "PRS Silver Sky",
        price: 2800.00,
        colors: [
            {
            code: "purple",
            img: "./img/prsSilver.png",
            },
            {
            code: "Rose",
            img: "./img/prsRose.png",
            },
        ],
        },
        {
        id: 4,
        title: "Epiphone Tom Delonge Signature ES-333",
        price: 1000,
        colors: [
            {
            code: "Green",
            img: "./img/tomDelong.png",
            },
            {
            code: "lightgray",
            img: "./img/tomDelong2.png",
            },
        ],
        },
        {
        id: 5,
        title: "Fender John Mayer BLK1",
        price: 4000,
        colors: [
            {
            code: "black",
            img: "./img/blk1.png",
            },
            // {
            // // code: "black",
            // // img: "./img/hippie2.png",
            // // },
        ],
        },
    ];

    let choosenProduct = [0]

    const currentProductImg = document.querySelector(".productImg");
    const currentProductTitle = document.querySelector(".productTitle");
    const currentProductPrice = document.querySelector(".productPrice");
    const currentProductColors = document.querySelectorAll(".color");
    const currentProductSizes = document.querySelectorAll(".size");
    

menuItems.forEach((item,index)=>{
    item.addEventListener("click", () => {
        wrapper.style.transform = `translateX(${-100 * index}vw)`;
        choosenProduct = products[index]
        currentProductTitle.textContent = choosenProduct.title
        currentProductPrice.textContent = "$" + choosenProduct.price
        currentProductImg.src = choosenProduct.colors[0].img

        currentProductColors.forEach((color,index)=>{
            color.style.backgroundColor = choosenProduct.colors[index].code;
        })
    });
});

currentProductColors.forEach((color,index)=>{
    color.addEventListener("click", ()=>{
        currentProductImg.src = choosenProduct.colors[index].img
    })
})

currentProductSizes.forEach((size,index)=>{
    size.addEventListener("click",()=>{
        currentProductSizes.forEach(size=>{
            size.style.backgroundColor = "white"
            size.style.color = "black"
        })
        size.style.backgroundColor = "black"
        size.style.color = "white"
    })
})